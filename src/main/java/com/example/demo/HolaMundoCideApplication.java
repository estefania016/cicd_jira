package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolaMundoCideApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolaMundoCideApplication.class, args);
		System.out.println("hola mundo");
	}

}
